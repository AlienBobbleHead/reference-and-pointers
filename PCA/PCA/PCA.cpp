// PCA.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

int val1 = 1;
int val2 = 2;
int* valPtr = &val1;
int& valRef = val1;


//////////////////////////////////////////////////////////////////////////
// Prints Values using different methods
//////////////////////////////////////////////////////////////////////////
void PrintValues()
{
	std::cout << std::endl << "==Printing Values==" << std::endl << std::endl;
	std::cout << "Value1 Value: " << val1 << " Value2 Value: " << val2 << " Pointer Value: " << *valPtr << " Reference Value: " << valRef << std::endl;
	std::cout << std::endl << "==Printing Addresses==" << std::endl << std::endl;
	std::cout << "Value1 Address: " << &val1 << " Value2 Address: " << &val2 << " Pointer Address: " << valPtr << " Reference Address: " << &valRef << std::endl;
}

int main()
{
	PrintValues();

	val1 = 5;

	std::cout << std::endl << "Something has changed!!" << std::endl << std::endl;

	PrintValues();

	valPtr = &val2;

	std::cout << std::endl << "Something has changed!!" << std::endl << std::endl;

	PrintValues();

	*valPtr = 3;

	std::cout << std::endl << "Something has changed!!" << std::endl << std::endl;

	PrintValues();

	valRef = 4;

	std::cout << std::endl << "Something has changed!!" << std::endl << std::endl;
	PrintValues();
    return 0;
}

